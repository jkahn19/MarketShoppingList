from http import HTTPStatus

from django.test import TestCase
from django.urls import reverse

from .models import Ingredient, Recipe, IngredientDetail
from .choices import CATEGORIES

class IngredientModelTest(TestCase):
    def test_ingredient_creation(self):
        """
            test a ingredients creation on the database
        """
        Ingredient(name='Ingredient',categorie='V').save()

        ingredients = Ingredient.objects.all()
        self.assertEquals(ingredients.count(), 1)

        ingredient = ingredients[0]
        self.assertEquals(ingredient.name, "Ingredient")
        self.assertEquals(ingredient.categorie, "V")

    def test_no_ingredients(self):
        """
            test a ingredients list views with no ingredients
        """
        response = self.client.get(reverse("ingredients_list"))
        self.assertContains(response, "No ingredients avaible", html=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        for choice in CATEGORIES:
            self.assertQuerySetEqual(response.context[choice[1]], [])


class RecipeModelTest(TestCase):
    def test_recipe_creation(self):
        """
            test a recipe creation on the database
        """
        Recipe(name='Recipe').save()

        recipes = Recipe.objects.all()
        self.assertEquals(recipes.count(), 1)

        recipe = recipes[0]
        self.assertEquals(recipe.name, "Recipe")

    def test_no_recipes(self):
        """
            test a recipes list views with no recipes
        """
        response = self.client.get(reverse("recipes_list"))
        self.assertContains(response, "No recipes avaible", html=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertQuerySetEqual(response.context["recipes_list"], [])


class IngredientDetailModelTest(TestCase):
    def test_ingredient_detail_creation(self):
        """
            test a ingredients detail on the data base from a recipe and an ingredients
        """
        ingredient = Ingredient(name='Ingredient', categorie='V')
        ingredient.save()

        recipe = Recipe(name='Recipe')
        recipe.save()

        IngredientDetail(ingredient=ingredient, recipe=recipe, unity='un', value=1).save()

        details = IngredientDetail.objects.all()
        self.assertEquals(details.count(), 1)

        detail = details[0]
        self.assertEquals(detail.ingredient, ingredient)
        self.assertEquals(detail.recipe, recipe)
        self.assertEquals(detail.unity, 'un')
        self.assertEquals(detail.value, 1)

    def test_no_ingredients_on_recipe(self):
        """
            test a ingredients list on a recipe views with no ingredients
        """
        Recipe(name='Recipe').save()
        recipe = Recipe.objects.all()[0]

        response = self.client.get(reverse("recipe", args=[recipe.id]))
        self.assertContains(response, "No ingredients on the recipe avaible", html=True)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response.context["recipe"], recipe)
        self.assertQuerySetEqual(response.context["ingredients_detail"], [])
