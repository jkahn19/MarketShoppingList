from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Ingredient, Recipe, IngredientDetail, MarketList
from .choices import CATEGORIES, UNITIES

class SignUpForm(UserCreationForm):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email Address'}))
    first_name = forms.CharField(label='', max_length=50, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Frist Name'}))
    last_name = forms.CharField(label='', max_length=50, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Last Name'}))

    class Meta:
        model = User
        fields = {'username', 'first_name', 'last_name', 'email', 'password1', 'password2'}

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['placeholder'] = 'User Name'
        self.fields['username'].label = ''
        self.fields['username'].help_text = '<span class="form-text text-muted"><small>Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.</small></span>'

        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Password'
        self.fields['password1'].label = ''
        self.fields['password1'].help_text = '<ul class="form-text text-muted small"><li>Your password can\'t be too similar to your other personal information.</li><li>Your password must contain at least 8 characters.</li><li>Your password can\'t be a commonly used password.</li><li>Your password can\'t be entirely numeric.</li></ul>'

        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'Confirm Password'
        self.fields['password2'].label = ''
        self.fields['password2'].help_text = '<span class="form-text text-muted"><small>Enter the same password as before, for verification.</small></span>'

class AddIngredient(forms.ModelForm):
    name = forms.CharField(required=True, label='', max_length=20, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ingredient name'}))
    categorie = forms.ChoiceField(required=True, label='', choices=CATEGORIES)

    class Meta:
        model = Ingredient
        fields = {'user', 'name', 'categorie'}

class AddRecipe(forms.ModelForm):
    name = forms.CharField(required=True, label='', max_length=20, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Recipe name'}))

    class Meta:
        model = Recipe
        fields = {'user', 'name'}

class AddIngredientToRecipe(forms.ModelForm):
    ingredient = forms.ModelChoiceField(queryset=Ingredient.objects.all(), label='')
    unity = forms.ChoiceField(required=True, label='', choices=UNITIES)
    value = forms.IntegerField(required=True, label='')

    class Meta:
        model = IngredientDetail
        fields = {'ingredient', 'recipe', 'unity', 'value'}

class AddMarket(forms.ModelForm):
    name = forms.CharField(required=True, label='', max_length=20, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Market name'}))

    class Meta:
        model = MarketList
        fields = {'user', 'name'}

class AddRecipeToMarket(forms.ModelForm):
    recipe = forms.ModelMultipleChoiceField(queryset=Recipe.objects.all(), widget=forms.CheckboxSelectMultiple, label='')

    class Meta:
        model = MarketList
        fields = {'recipe'}