from django.db import models
from django.contrib.auth.models import User

from .choices import CATEGORIES, UNITIES


class Ingredient(models.Model):
    """ A class to represent a ingredient of differents kitchen recipe

    Attributes:
        name (string) : ingredient name
            constraints {
                max_length : 20
                unique : True
            }
        user (class User): User Foreing key
        categorie (string) : ingredient categorie (ex Fruits, vegetables)
            constraints {
                max_length : 1
                default : 'V'
            }
    """

    name = models.CharField(max_length=20, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    categorie = models.CharField(max_length=1, default='V', choices=CATEGORIES)

    def __str__(self):
        return f'{self.name}'

class Recipe(models.Model):
    """A class to represent a kitchen recipe

    Attributes:
        name (string): recipe name
            constraints {
                max_length : 20
                unique : True
            }
        user (class User): User Foreing key
        ingredients (class Ingredient): an ingredient
    """
    name = models.CharField(max_length=20, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ingredients = models.ManyToManyField(Ingredient, through='IngredientDetail')

    def __str__(self):
        return f'{self.name}'

class IngredientDetail(models.Model):
    """A class to represent the ingredient detail of a kitchen recipe

    Attributes:
        pk_ingredient (int) : ingredient foreign key
        pk_recipe (int) : recipe foreign key.
        unity (string) : ingredient quantity unit, two differents option gr and un.
            constraints {
                max_length : 2
                default : 'gr'
            }
        value (int) : quantity value.
            constraints {
                default : 0
            }
    """
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    unity = models.CharField(max_length=2, default='gr', choices=UNITIES)
    value = models.IntegerField(default=0, null=False)

    def __str__(self):
        return "{}_{}".format(self.recipe.__str__(), self.ingredient.__str__())


class MarketList(models.Model):
    """A class to represent a market shopping list, with contains all the recipe

    Attribute:
        name (string): market list name
        user (class): user foreign key
        recipe (class Recipe): recipe foreign key
    """
    name = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ManyToManyField(Recipe)

    def __str__(self):
        return f'{self.name}'

    def ingredients_list(self):
        list_detail = {}

        recipes = self.recipe.all()
        for each_recipe in recipes:
            ingredients = IngredientDetail.objects.filter(recipe=each_recipe)
            for ingredient in ingredients:
                ingredients_id = ingredient.ingredient.id
                gr_value, un_value = (ingredient.value,0) if ingredient.unity == UNITIES[0][0] else (0,ingredient.value)
                if ingredients_id in list_detail.keys():
                    list_detail[ingredients_id][UNITIES[0][0]] += gr_value
                    list_detail[ingredients_id][UNITIES[1][0]] += un_value

                else:
                    list_detail[ingredients_id] = {
                        'name': ingredient.ingredient.name,
                        UNITIES[0][0]: gr_value,
                        UNITIES[1][0]: un_value
                    }
        return list(list_detail.values())