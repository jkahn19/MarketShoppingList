from django.urls import path

from .views import home, market, add_market, delete_market, update_market
from .views import register_user, logout_user
from .views import ingredients_list, add_ingredient, delete_ingredient
from .views import recipes_list, recipe, update_recipe, add_recipe, delete_recipe
from .views import add_ingredient_to_recipe, delete_ingredient_from_recipe
from .views import add_recipe_to_market, delete_recipe_from_market
from .views import generate_list
# from .views import market

urlpatterns = [
    path('', home, name='home'),
    path('add_market/',add_market,name='add_market'),
    path('market/<int:pk>/',market,name='market'),
    path('delete_market/<int:pk>/',delete_market,name='delete_market'),
    path('update_market/<int:pk>/',update_market,name='update_market'),
    path('register/', register_user, name='register'),
    path('logout/', logout_user, name='logout'),
    path('ingredients_list/', ingredients_list, name='ingredients_list'),
    path('add_ingredient/', add_ingredient, name='add_ingredient'),
    path('delete_ingredient/<int:pk>/', delete_ingredient, name='delete_ingredient'),
    path('recipes_list/', recipes_list, name='recipes_list'),
    path('add_recipe/', add_recipe, name='add_recipe'),
    path('recipe/<int:pk>/', recipe, name='recipe'),
    path('update_recipe/<int:pk>/', update_recipe, name='update_recipe'),
    path('delete_recipe/<int:pk>/', delete_recipe, name='delete_recipe'),
    path('add_ingredient_to_recipe/<int:pk>/', add_ingredient_to_recipe, name='add_ingredient_to_recipe'),
    path('delete_ingredient_from_recipe/<int:pk_recipe>/<int:pk_ingredient>/', delete_ingredient_from_recipe, name='delete_ingredient_from_recipe'),
    path('add_recipe_to_market/<int:pk>/', add_recipe_to_market, name='add_recipe_to_market'),
    path('delete_recipe_from_market/<int:pk_market>/<int:pk_recipe>/', delete_recipe_from_market, name='delete_recipe_from_market'),
    path('generate_list/<int:pk>/', generate_list, name='generate_list'),
]
