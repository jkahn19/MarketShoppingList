from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from .forms import AddIngredient, AddRecipe, AddIngredientToRecipe, SignUpForm, AddMarket, AddRecipeToMarket
from .models import Ingredient, Recipe, IngredientDetail, MarketList

from .choices import CATEGORIES


def home(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        #authenticate
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            messages.success(request, "You have been logged in!")
            return redirect('home')
        else:
            messages.success(request, "There was an Error Logging In, Please try again.")
            return render(request, 'home.html', {})
    else:
        market_list = None
        if request.user.is_authenticated:
            market_list = MarketList.objects.filter(user=request.user)
        return render(request, 'home.html', {'market_list': market_list})

def add_market(request):
    if request.user.is_authenticated:
        form = AddMarket(request.POST or None, initial={'user': User.objects.get(pk=request.user.id)})
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You added a new market list')
                return redirect('home')
        return render(request, 'add_market.html', {'form': form})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')


def market(request, pk):
    if request.user.is_authenticated:
        market_list = MarketList.objects.get(id=pk, user=request.user)
        return render(request, 'market_list.html', {'market_list': market_list})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def update_market(request, pk):
    if request.user.is_authenticated:
        current_market = MarketList.objects.get(id=pk, user=request.user)
        form = AddRecipe(request.POST or None, instance=current_market)
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You updated a market')
                return redirect('home')
        return render(request, 'update_market.html', {'form': form})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')


def delete_market(request, pk):
    if request.user.is_authenticated:
        market = MarketList.objects.get(id=pk, user=request.user)
        market.delete()
        return redirect('home')
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def logout_user(request):
    logout(request)
    messages.success(request, "You have been logged Out!")
    return redirect('home')

def register_user(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, 'You have successfully registered! Welcome!')
            return redirect('home')
    else:
        form = SignUpForm()
        return render(request, 'register.html', {'form':form})

    return render(request, 'register.html', {'form':form})

def ingredients_list(request):
    if request.user.is_authenticated:
        ingredients = Ingredient.objects.filter(user=request.user)
        context = {}
        for choice in CATEGORIES:
            context[choice[1]] = ingredients.filter(categorie=choice[0])
        return render(request, 'ingredients_list.html', context)
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def add_ingredient(request):
    if request.user.is_authenticated:
        form = AddIngredient(request.POST or None, initial={'user': User.objects.get(pk=request.user.id)})
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You added a new ingredients')
                return redirect('ingredients_list')
        return render(request, 'add_ingredient.html', {'form': form})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def delete_ingredient(request, pk):
    if request.user.is_authenticated:
        ingredient = Ingredient.objects.get(id=pk, user=request.user)
        ingredient.delete()
        return redirect('ingredients_list')
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def recipes_list(request):
    if request.user.is_authenticated:
        recipes_list = Recipe.objects.filter(user=request.user)
        return render(request, 'recipes_list.html', {'recipes_list': recipes_list})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def add_recipe(request):
    if request.user.is_authenticated:
        form = AddRecipe(request.POST or None, initial={'user': User.objects.get(pk=request.user.id)})
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You added a new recipe')
                return redirect('recipes_list')
        return render(request, 'add_recipe.html', {'form': form})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def delete_recipe(request, pk):
    if request.user.is_authenticated:
        recipe = Recipe.objects.get(id=pk, user=request.user)
        recipe.delete()
        return redirect('recipes_list')
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def update_recipe(request, pk):
    if request.user.is_authenticated:
        current_recipe = Recipe.objects.get(id=pk, user=request.user)
        form = AddRecipe(request.POST or None, instance=current_recipe)
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You updated a recipe')
                return redirect('recipes_list')
        return render(request, 'update_recipe.html', {'form': form})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def recipe(request, pk):
    if request.user.is_authenticated:
        recipe = Recipe.objects.get(id=pk, user=request.user)
        ingredients_detail = IngredientDetail.objects.filter(recipe=recipe.id)
        return render(request, 'recipe.html', {'recipe': recipe, 'ingredients_detail': ingredients_detail})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def add_ingredient_to_recipe(request, pk):
    if request.user.is_authenticated:
        recipe = Recipe.objects.get(id=pk, user=request.user)
        form = AddIngredientToRecipe(request.POST or None, initial={'recipe': recipe,})
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You added an ingredient to the recipe')
                return redirect('recipe',pk)
        return render(request, 'add_ingredient_to_recipe.html', {'form': form, 'recipe': recipe})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def delete_ingredient_from_recipe(request, pk_recipe, pk_ingredient):
    if request.user.is_authenticated:
        ingredient_detail = IngredientDetail.objects.get(ingredient=pk_ingredient,recipe=pk_recipe)
        ingredient_detail.delete()
        return redirect('recipe', pk_recipe)
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def add_recipe_to_market(request, pk):
    if request.user.is_authenticated:
        current_market = MarketList.objects.get(id=pk, user=request.user)
        form = AddRecipeToMarket(request.POST or None, instance=current_market)
        if request.method == 'POST':
            if form.is_valid():
                form.save()
                messages.success(request, 'You added a recipe to market list')
                return redirect('market', pk)
        return render(request, 'add_recipe_to_market.html', {'form': form, 'market_list': current_market})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def delete_recipe_from_market(request, pk_market, pk_recipe):
    if request.user.is_authenticated:
        current_recipe = Recipe.objects.get(id=pk_recipe, user=request.user)
        current_market = MarketList.objects.get(id=pk_market, user=request.user)
        current_market.recipe.remove(current_recipe)
        return redirect('market', pk_market)
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')

def generate_list(request, pk):
    if request.user.is_authenticated:
        market = MarketList.objects.get(id=pk, user=request.user)
        shopping_list = market.ingredients_list()
        return render(request, 'generate_list.html', {'market': market, 'shopping_list': shopping_list})
    else:
        messages.success(request, 'You must be logged in to add new record.')
        return redirect('home')