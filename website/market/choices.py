
CATEGORIES = [
        ("V", "Vegetables"),
        ("M", "Meat"),
        ("D", "Dairy"),
        ("C", "Condiments"),
        ("P", "Pasta"),
        ("F", "Fruits"),
    ]

UNITIES = [
    ('gr', 'Grams'),
    ('un', 'Units'),
]