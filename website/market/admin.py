from django.contrib import admin

from .models import Ingredient, Recipe, IngredientDetail, MarketList

admin.site.register(Ingredient)
admin.site.register(Recipe)
admin.site.register(IngredientDetail)
admin.site.register(MarketList)